/**
 * @file getaddrinfo_main.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief 
 * @version 0.1.1.SNAPSHOT
 * @date 2020-02-16
 * 
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include <err.h>
#include <sysexits.h>
#include <getopt.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum limited_strtoll_status {
    LS_INIT = 0x0,
    LS_DID_NOT_END = 0x1,
    LS_UNDERFLOW = 0x2,
    LS_OVERFLOW = 0x4,
    LS_INVALID = 0x8,
};

long long
limited_strtoll(const char *str, char **endp, int base,
    long long min_ll, long long max_ll, enum limited_strtoll_status *status)
{
    enum limited_strtoll_status inner_status;
    if (status == NULL) {
        status = &inner_status;
    }
    *status = LS_INIT;
    char *endpoint;
    if (endp == NULL) {
        endp = &endpoint;
    }
    errno = 0;
    long long ans = strtoll(str, endp, base);
    int saved_errno = errno;
    if (saved_errno == ERANGE) {
        switch (ans) {
        case LLONG_MIN:
            *status = LS_UNDERFLOW;
            break;
        case LLONG_MAX:
            *status = LS_OVERFLOW;
            break;
        }
    } else if (saved_errno != 0 && ans == 0) {
        *status = LS_INVALID;
    }
    if (ans < min_ll) {
        *status |= LS_UNDERFLOW;
    } else if (ans > max_ll) {
        *status |= LS_OVERFLOW;
    }
    if (**endp) {
        *status |= LS_DID_NOT_END;
    }
    return ans;
}

int
str_to_int(const char *const str, int base, enum limited_strtoll_status *status)
{
    if (str == NULL) {
        return 0;
    }
    char *endp;
    return limited_strtoll(str, &endp, base,
        (long long)INT_MIN, (long long)INT_MAX, status);
}

void
usage(FILE *fp)
{
    fputs("Usage:\n"
    " getaddrinfo [--help|--version]\n"
    " getaddrinfo [--service=service_name] [--protocol=protocol]"
    " [--address-family=address_family] [--socket-type=socket_type]"
    " [--flags=flags] [host_name]\n", fp);
}

void
version(void)
{
    fputs("0.1.0\n", stdout);
}

const char * const
print_string(const char *const str)
{
    return (str) ? str : "null";
}

void
dump(size_t size, unsigned char *content)
{
    for (size_t n = 0; n < size; ++n) {
        printf("%02X(%u).", content[n], content[n]);
    }
}

void *
response_thread(void *arg)
{
    int *socket_number = arg;
    FILE *read_fp = fdopen(*socket_number, "rb");
    if (read_fp == NULL) {
        err(EX_OSERR, "fdopen(%d, rb) errno %d", *socket_number, errno);
        /*NOTREACHED*/
    }
    size_t read_size;
    char read_buffer[BUFSIZ];
    while ((read_size = fread(read_buffer, 1u, sizeof(read_buffer),
    read_fp)) > 0u) {
        if (fwrite(read_buffer, read_size, 1u, stdout) < 1) {
            err(EX_IOERR, "fwrite(%p, %llu, 1, stdout) = 0, errno %d",
                (void *)read_buffer, (unsigned long long)read_size, errno);
                /*NOTREACHED*/
        }
        if (fflush(stdout)) {
            err(EX_IOERR, "fflush(stdout) errno %d", errno);
            /*NOTREACHED*/
        }
    }

    return NULL;
} 

int
get_address_info(const char *const host_name, const char *const service_name,
    const struct addrinfo *const hints, struct addrinfo **resp)
{
    printf("host %s, service %s\n",
     print_string(host_name), print_string(service_name));
    int ret = getaddrinfo(host_name, service_name, hints, resp);
    if (ret) {
        warnx("%d:%s", ret, gai_strerror(ret));
        return ret;
    }
    int n = 0;
    for (struct addrinfo *current = *resp; current;
    current = current->ai_next) {
        ++n;
        printf(
            "flags %x, family %d, type %d, protocol %d, canon %s, len %u, ",
            current->ai_flags, current->ai_family, current->ai_socktype,
            current->ai_protocol, print_string(current->ai_canonname),
            current->ai_addrlen);
            dump(current->ai_addrlen, (unsigned char *)&current->ai_addr);
            fputc('\n', stdout);
    }
    return n;
}

int
main(int argc, char *argv[])
{
    char *host_name = NULL;
    char *service_name = NULL;
    char *protocol_string = NULL;
    char *socket_type_string = NULL;
    char *address_family_string = NULL;
    char *flags_string = NULL;

    for (;;) {
        int option_index = 0;
        static const struct option long_options[] = {
            { "help", no_argument, 0, 'h' },
            { "version", no_argument, 0, 'V' },
            { "service", required_argument, 0, 's' },
            { "protocol", required_argument, 0, 'p' },
            { "socket-type", required_argument, 0, 'c' },
            { "address-family", required_argument, 0, 'a' },
            { "flags", required_argument, 0, 'f' },
            { NULL, 0, NULL, 0 },
        };
        int c = getopt_long(argc, argv, "hVs:p:c:a:f:",
         long_options, &option_index);
        if (c == -1) {
            break;
        }
        switch (c) {
        case 'h':
            usage(stdout);
            return 0;
            break;
        case 'V':
            version();
            return 0;
            break;
        case 's':
            service_name = optarg;
            break;
        case 'p':
            protocol_string = optarg;
            break;
        case 'c':
            socket_type_string = optarg;
            break;
        case 'a':
            address_family_string = optarg;
            break;
        case 'f':
            flags_string = optarg;
            break;
        case '?':
            usage(stderr);
            return EX_USAGE;
            break;
        default:
            usage(stderr);
            assert(0);
            return EX_USAGE;
            break;
        }
    }
    if (optind < argc) {
        host_name = argv[optind];
    }
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    enum limited_strtoll_status status_of_protocol = LS_INIT;
    hints.ai_protocol = str_to_int(protocol_string, 10, &status_of_protocol);
    if (status_of_protocol) {
        errx(EX_DATAERR, "bad protocol %s", protocol_string); /*NOTREACHED*/
    }
    enum limited_strtoll_status status_of_socket_type = LS_INIT;
    hints.ai_socktype = str_to_int(socket_type_string, 10,
     &status_of_socket_type);
     if (status_of_socket_type) {
         errx(EX_DATAERR, "bad socket type %s", socket_type_string);
         /*NOTREACHED*/
     }
    enum limited_strtoll_status status_of_address_family = LS_INIT;
    hints.ai_family = str_to_int(address_family_string, 10,
     &status_of_address_family);
     if (status_of_socket_type) {
         errx(EX_DATAERR, "bad address family %s", address_family_string);
         /*NOTREACHED*/
     }
    enum limited_strtoll_status status_of_flags = LS_INIT;
    hints.ai_flags = str_to_int(flags_string, 16, &status_of_flags);
    if (status_of_flags) {
        errx(EX_DATAERR, "bad flags %s", flags_string);  /*NOTREACHED*/
    }

    int n;
    struct addrinfo *resp;
    if ((n = get_address_info(host_name, service_name, &hints, &resp)) < 0) {
        return EX_NOHOST;
    }
    if (n > 1) {
        return EX_UNAVAILABLE;
    }
    int socket_number = socket(resp->ai_family, resp->ai_socktype,
                            resp->ai_protocol);
    if (socket_number < 0) {
        err(EX_SOFTWARE, "socket(%d, %d, %d) = %d, errno %d", resp->ai_family,
            resp->ai_socktype, resp->ai_protocol, socket_number, errno);
            /*NOTREACHED*/
    }
    if (connect(socket_number, resp->ai_addr, resp->ai_addrlen)) {
        err(EX_OSERR, "connect(%d, %p, %llu) errno %d", socket_number,
            (void *)resp->ai_addr, (unsigned long long)resp->ai_addrlen,
             errno); /*NOTREACHED*/
    }
    FILE *write_fp = fdopen(socket_number, "wb");
    if (write_fp == NULL) {
        err(EX_SOFTWARE, "fdopen(%d, wb) errno %d", socket_number, errno);
        /*NOTRERACHED*/
    }
    pthread_t thread;
    int create_error;
    if ((create_error = pthread_create(&thread, NULL, response_thread,
    &socket_number)) != 0) {
        errno = create_error;
        err(EX_OSERR, "pthread_create(%p, NULL, %p, %d) = %d",
            (void *)&thread, (void *)response_thread, socket_number,
            create_error);
        /*NOTREACHED*/
    }
    
    char input_buffer[BUFSIZ];
    while (fgets(input_buffer, sizeof(input_buffer), stdin)) {
        size_t read_size = strlen(input_buffer);
        if (fwrite(input_buffer, read_size, 1, write_fp) != 1) {
            err(EX_IOERR, "fwrite(%p, %llu, 1, %p) != 1, errno %d",
                (void *)input_buffer, (unsigned long long)read_size,
                (void *)write_fp, errno); /*NOTREACHED*/
        }
        if (fflush(write_fp)) {
            err(EX_IOERR, "fflush(write_fp) errno %d", errno); /*NOTREACHED*/
        }
    }
    int join_error;
    if ((join_error = pthread_join(thread, NULL)) != 0) {
        errno = join_error;
        err(EX_IOERR, "pthread_join(%lld, NULL) = %d",
            (unsigned long long)thread, join_error);
        /*NOTREACHED*/
    }
    return EX_OK;
}